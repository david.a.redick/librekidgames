#!/bin/sh

# usage: add-spaces-between-letters.sh text.txt > text-with-spaces.txt

# In order to make the letters more distinct, it may be necessary to
# add spaces between them.
#
# Released into the public domain where possible or under the Creative Commons Zero license (CC0)
# https://creativecommons.org/publicdomain/zero/1.0/


# add one space after every character (letters, symbols, numbers, spaces)
sed 's/./& /g' $1
