#!/bin/bash

# funny-talk.bash

# A simple read-eval-print-loop program
# that allows you to type a line and we you hit enter
# it will send what you typed to the festival text-to-speech system.
# And keep doing so until you Ctrl-C or type 'exit'
#
# You'll need bash and festival installed.
# On Debian and the like: apt-get install bash festival
#
# Redick is using bash instead of sh for readline functionality.
#
# Released into the public domain where possible or under the Creative Commons Zero license (CC0)
# https://creativecommons.org/publicdomain/zero/1.0/


TEXT=''
until [ 'exit' = "$TEXT" ]; do

	# read some keyboard but with GNU readline so that things like the arrow keys work.
	read -e TEXT

	# yup... we load festival up everytime.
	# most distro's don't install the libraries so can't do that.
	# most distro's don't install the daemon so can't use that.
	# and festival's scheme interface is too limited.
	# the festival << exit style buffers until exit is typed
	# and this is just a kids toy
	festival --tts <<< "$TEXT"
done
